// -*- coding: utf-8 -*-

(function() {
    var form = $('form[name="LoginForm"]');
    if (form.length == 1) {
        var ckey = 'AGLOGINID';
        var c_agloginid = $.cookie(ckey);
        var l_agloginid = localStorage.agloginid;
        var l_password = localStorage.password;
        
        if (c_agloginid) {
            if (! l_agloginid) {
                localStorage.agloginid = c_agloginid;

                l_password = window.prompt('input password to login cybozu');
                if (l_password != null) {
                    localStorage.password = l_password;
                }
            }
            
            if (l_password) {
                $('input:password').val(l_password);
                form.submit();
            }
        } else {
            if (l_agloginid) {
                $.cookie(ckey, l_agloginid, {
                    expires: 30,
                    path: location.pathname.substr(0, location.pathname.length - 6),
                    domain: location.host
                });
                location.reload();
            }
        }
    }
}());
